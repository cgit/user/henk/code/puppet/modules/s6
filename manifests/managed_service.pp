define s6::managed_service (
  String $runscript,
  String $service_dir = $s6::service_dir,
  String $scan_dir = $s6::scan_dir,
  Optional[String] $finishscript = undef,
  Optional[String] $logger_runscript = undef,
) {
  file {
    "${service_dir}/${name}":
      ensure => directory,
    ;
    "${service_dir}/${name}/run":
      ensure  => present,
      content => $runscript,
      mode    => '0700',
    ;
  }
  if $finishscript {
    file {
      "${service_dir}/${name}/finish":
        ensure  => present,
        content => $finishscript,
        mode    => '0700',
      ;
    }
  }
  if $logger_runscript {
    file {
      "/var/log/s6/${name}":
        ensure => directory,
      ;
      "${service_dir}/${name}/log":
        ensure => directory,
      ;
      "${service_dir}/${name}/log/run":
        ensure  => present,
        content => $logger_runscript,
        mode    => '0700',
      ;
    }
  }
}
