define s6::user_scandir(
  Optional[String] $homedir = undef,
) {
  $scandir = '/etc/s6-scandir'

  file {
    "${scandir}/user_scandir_${title}":
      ensure => directory,
      ;
    "${scandir}/user_scandir_${title}/run":
      ensure  => present,
      content => epp('s6/user_scandir/run.epp', { 'username' => $title } ),
      mode    => '0744',
      ;
    "${scandir}/user_scandir_${title}/log":
      ensure => directory,
      ;
    "${scandir}/user_scandir_${title}/log/run":
      ensure  => present,
      content => epp('s6/user_scandir/log/run.epp', { 'username' => $title } ),
      mode    => '0744',
      ;
    [
      "/var/log/s6/user_scandir_${title}/",
      "/var/log/s6/user_scandir_${title}/logs/",
    ]:
      ensure => directory,
    ;
  }
}

