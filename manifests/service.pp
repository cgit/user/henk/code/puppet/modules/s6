class s6::service {
  require 's6::logrotater'
  service {
    's6':
      ensure => running,
      enable => true,
    ;
    's6-logrotater':
      ensure   => running,
      enable   => true,
      provider => 's6',
    ;
  }
}
