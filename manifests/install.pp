class s6::install {
  package {
    's6':
      ensure => installed,
    ;
  }
  file {
    '/etc/s6-services/':
      ensure => directory,
    ;
    '/etc/s6-scandir/':
      ensure => directory,
    ;
    '/var/log/s6/':
      ensure => directory,
      mode   => '0700',
    ;
  }

  if $facts['service_provider'] == 'systemd' {
    file {
      '/etc/systemd/system/s6.service':
        ensure  => present,
        source  => 'puppet:///modules/s6/etc/systemd/system/s6.service',
        require => Package['s6'],
        notify  => Class['s6::service'],
      ;
    }
  } else {
    file {
      '/usr/local/bin/s6-svscanboot':
        ensure  => present,
        source  => [
          "puppet:///modules/s6/usr/local/bin/s6-svscanboot.${facts['os']['distro']['codename']}",
          'puppet:///modules/s6/usr/local/bin/s6-svscanboot',
        ],
        mode    => '0755',
        require => Package['s6'],
        notify  => Class['s6::service'],
      ;
      '/etc/init.d/s6':
        ensure  => present,
        source  => 'puppet:///modules/s6/etc/init.d/s6',
        mode    => '0755',
        require => Package['s6'],
        notify  => Class['s6::service'],
      ;
    }
  }
  user {
    's6-logs':
      ensure => present,
      home   => '/nonexistant',
      shell  => '/usr/sbin/nologin',
      system => true,
    ;
  }
}
