class s6::logrotater {
  $basedir = '/etc/s6-services/s6-logrotater'

  file {
    $basedir:
      ensure => directory,
      mode   => '0600',
    ;

    "${basedir}/run":
      ensure  => present,
      content => epp("s6/${basedir}/run.epp"),
      mode    => '0700',
    ;
  }
}
