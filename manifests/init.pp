class s6 (
  Optional[Array[String]] $scandir_users = [],
  String $service_dir                    = '/etc/s6-services/',
  String $scan_dir                       = '/etc/s6-scandir/',
) {

  class {
    "${name}::install": ;
    "${name}::snippets": ;
    "${name}::service": ;
    "${name}::scandir_users": ;
  }
}
