Puppet::Type.type(:service).provide(:s6) do
  desc <<~EOT
    skarnet.org's small and secure supervision software suite

    The scan directory is the first one found of:
      * /etc/s6-scandir/

    The first matching service definition found in the following directories
    is used:
      * /etc/s6-services/


    This provider supports:

    * start/stop
    * enable/disable
    * restart
    * status
  EOT

  commands :s6_svscan    => 's6-svscan'
  commands :s6_svscanctl => 's6-svscanctl'
  commands :s6_svc       => 's6-svc'
  commands :s6_svstat    => 's6-svstat'

  class << self
    def specificity
      return 1
    end

    attr_writer :defpath

    # this is necessary to autodetect a valid resource
    # default path, since there is no standard for such directory.
    def defpath
      @defpath ||= ["/etc/s6-services"].find do |path|
        Puppet::FileSystem.exist?(path) && FileTest.directory?(path)
      end
      @defpath
    end

    # returns all providers for all existing services in @defpath
    # ie enabled or not
    def instances
      path = self.defpath
      unless path
        Puppet.info("#{self.name} is unsuitable because service directory is nil")
        return
      end
      unless FileTest.directory?(path)
        Puppet.notice "Service path #{path} does not exist"
        return
      end

      # reject entries that aren't either a directory
      # or don't contain an executable run file
      Dir.entries(path).reject { |e|
        fullpath = File.join(path, e)
        e =~ /^\./ or ! FileTest.directory?(fullpath) or ! Puppet::FileSystem.executable?(File.join(fullpath,"run"))
      }.collect do |name|
        new(:name => name, :path => path)
      end
    end

    # returns the daemon dir on this node
    def daemondir
      self.defpath
    end
  end

  attr_writer :servicedir

  # find the service dir on this node
  def servicedir
    unless @servicedir
      ["/etc/s6-scandir"].each do |path|
        if Puppet::FileSystem.exist?(path) && FileTest.directory?(path)
          @servicedir = path
          break
        end
      end
      raise "Could not find service scan directory" unless @servicedir
    end
    @servicedir
  end

  # returns the full path of this service when enabled
  # (ie in the service directory)
  def service
    File.join(self.servicedir, resource[:name])
  end

  # returns the full path to the current daemon directory
  # note that this path can be overridden in the resource
  # definition
  def daemon
    path = resource[:path]
    raise Puppet::Error.new("#{self.class.name} must specify a path for daemon directory") unless path
    File.join(path, resource[:name])
  end

  def status
    begin
      output = s6_svstat "-u", self.service
      if output.chomp == 'true'
        return :running
      end
    rescue Puppet::ExecutionFailure => detail
      unless detail.message =~ /(warning: |s6-supervise not running$)/
        raise Puppet::Error.new( "Could not get status for service #{resource.ref}: #{detail}", detail )
      end
    end
    :stopped
  end

  def enabled?
    # the service is enabled if it is linked
    case Puppet::FileSystem.symlink?(self.service) ? :true : :false
    when :true
      return :true
    when :false
      return :false
    else
      raise Puppet::Error.new( "Received unknown state for #{self.service}", $!)
    end
  end

  def enable
    if ! FileTest.directory?(self.daemon)
      Puppet.notice "No daemon dir, calling setupservice for #{resource[:name]}"
    end
    if self.daemon
      if ! Puppet::FileSystem.symlink?(self.service)
        Puppet.notice "Enabling #{self.service}: linking #{self.daemon} -> #{self.service}"
        Puppet::FileSystem.symlink(self.daemon, self.service)
      end
    end
    s6_svscanctl "-a", self.servicedir
  rescue Puppet::ExecutionFailure
    raise Puppet::Error.new( "No daemon directory found for #{self.service}", $!)
  end

  def disable
    # unlink the daemon symlink to disable it
    Puppet::FileSystem.unlink(self.service) if Puppet::FileSystem.symlink?(self.service)
    s6_svscanctl "-n", self.servicedir
  end

  def restart
    s6_svc "-r", self.service
  end

  def start
    if enabled? != :true
        enable
    end
    s6_svc "-u", self.service
  end

  def stop
    s6_svc "-d", self.service
  end
end
